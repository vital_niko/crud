<?php
return [
    'crud_tooltip' => [
        'pattern' => '<i class="fa fa-question-circle" data-crud_tooltip="%s"></i>',
        'pattern_static' => '<i class="fa fa-question-circle" data-crud_tooltip="dummy" data-crud_tooltip_text="%s"></i>',
        'acl' => 'tooltips'
    ]
];