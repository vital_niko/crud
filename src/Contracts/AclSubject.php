<?php namespace LaravelCrud\Contracts;

interface AclSubject
{
    function getAcls();
}